class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :connect_api

  private

  def connect_api
    @api = Libring::Report.new(Rails.application.secrets.libring_token)
  end
end
