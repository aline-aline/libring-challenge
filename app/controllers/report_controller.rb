class ReportController < ApplicationController
  def index
    @results = @api.make_request_with(request_params)
  end

  private

  def request_params
    params.permit(:start_date, :end_date, :app, :data_type, :period, group_by: []).to_h
  end
end
