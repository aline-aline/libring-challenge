require 'net/https'
require 'uri'

module Libring
  class Report
    BASE_URI = 'https://api.libring.com/v2/reporting/get'

    def initialize(key)
      @uri = URI(BASE_URI)
      @https = Net::HTTP.new(@uri.host, @uri.port)
      @https.use_ssl = true
      @header = { 'Content-Type' => 'application/json', 'Authorization' => key }
    end

    def make_request_with(params={})
      @uri.query = URI.encode_www_form(with_normalized(params))

      raise 'API Connection error!' unless @https.request(api_call).is_a?(Net::HTTPSuccess)

      if next_page_of(api_call).present?
        @results = []

        page_numbers_of(api_call).times do |i|
          next_report_from_api = Net::HTTP::Get.new(
            URI(next_page_of(api_call) + "&page=#{i+1}"), @header
          )
          @results += JSON.parse(@https.request(next_report_from_api).body)['connections']
        end

        @results
      else
        result_body_on_key('connections')
      end
    end

    private

    def with_normalized(params={})
      params[:group_by] << 'date' if params[:group_by]
      params[:group_by] = params[:group_by].join(",") if params[:group_by]
      params.delete(:period) if (params[:start_date].blank? || params[:end_date].blank?)
      params.symbolize_keys
    end

    def page_numbers_of(api_call)
      result_body_on_key('total_pages')
    end

    def next_page_of(api_call)
      result_body_on_key('next_page_url')
    end

    def api_call
      Net::HTTP::Get.new(@uri.request_uri, @header)
    end

    def result_body_on_key(key)
      JSON.parse(@https.request(api_call).body)[key]
    end
  end
end
