# libring-challenge

A simple web page to make requests to [Libring API](https://libring.com/reporting-api/).

## Used techniques

- Ruby 2.5
- Rails 5.1.5
- [Libring API](https://libring.com/reporting-api/)
- [Datatables](https://datatables.net/)

## Getting started

```
Clone this repository
bundle install
rails s
```
